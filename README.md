* Extrêmement simple à utiliser. Pour implémenter un slider sur votre site, comptez une trentaine de secondes.
* Un menu clair, épuré et dépourvu de toute option inutile. Et parfaitement traduit en français.
* Le responsive. Meta Slider s’adapte parfaitement sur tablettes et smartphones.

## options réglables :
* langue du titre : anglais, francais et arabe
* parametres : - nombre de slides maximum , vitesse, categorie
* style : Images Effect, Images Style, Images Transition, Carosel Indicators, Carosel Controls, Carosel Texte
