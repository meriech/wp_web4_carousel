<?php
/*
  Plugin Name: Web4 Carousel
  Plugin URI: http://web4.fr/wordpress/web4-plugings/web4-widget-carousel/
  Description: Web4_carousel is a carousel widget which shows images and titles from the categories with  several options : multilingual , category choise, number of  slides, speed, images effects, images layouts, transitions, show or hide arrows and bullets.
  Author: Amar MERIECH
  Version: 1.6
  Author URI: http://web4.fr/
 */

// Creating the widget 
class web4_widget_carousel extends WP_Widget {

//------------------------------------------------------------------------------
    function __construct() {
        parent::__construct(
                // Base ID of your widget
                'web4_widget_carousel',
                // Widget name will appear in UI                                      
                __('Web4 Widget Carousel', 'kalimates'),
                // Widget description
                array('description' => __('Web4 Widget Carousel bootstrap', 'kalimates'),)
        );
    }

//------------------------------------------------------------------------------
// Creating widget front-end
// This is where the action happens
    public function widget($args, $instance) {


        //self::$widget_id = $id;
        //echo self::$widget_id; //here outputs sidebar's id perfectly on my page

        $Default_cat_carousel = 11; //11;

        $ar_title = $instance['ar_title'];
        $fr_title = $instance['fr_title'];
        $en_title = $instance['en_title'];
        $locale = get_locale();
        switch ($locale) {
            case 'ar' : $title = $ar_title;
                break;
            case 'fr_FR' : $title = $fr_title;
                break;
            case 'en_US' : $title = $en_title;
                break;
            //default : $title = $ar_title; break;
        }
        //$title = apply_filters( 'widget_title', $instance['title'] );
        $title = apply_filters('widget_title', $title);
        //------------------------------------------------------------------------------
        $max_slides = $instance['max_slides'];
        if (!$max_slides) {
            $max_slides = 10;
        }
        $cat = $instance['cat'];
        $meta_key = $instance['meta_key'];
        $meta_value = $instance['meta_value'];
        $vitesse = $instance['vitesse'];
        if (!$vitesse) {
            $vitesse = 4000;
        }
        $effects = $instance['effects'];
        $images_style = $instance['images_style'];
        $images_transition = $instance['images_transition']; //face, rotate,zoom
        $carousel_indicators = $instance['carousel_indicators']; // oui /non
        $carousel_controls = $instance['carousel_controls']; // oui /non
        $carousel_texte = $instance['carousel_texte']; // oui /non
        // before and after widget arguments are defined by themes
        //------------------------------------------------------------------------------

        if (!$meta_key) {
            if (!$cat) { // si pas rensignée alors nous prenons la categorie en cours si yas des article sinon cat default
                if (is_category()) { // le carousel sans categorie mais on  y dans une categorie , on la prends
                    $category_name = single_cat_title("", false); //return name i need ID single_tag_title(); 
                    $category_id = get_query_var('cat'); // categorie zn cours de l'url
                    $category = get_queried_object();
                    $category_nbposts = $category->count;
                    $title = $category_name . "/id=" . $category_id . "/nb=" . $category_nbposts . "reel";
                    $title = $category_name;
                    $cat = $category_id;
                } else {  // cas tag  ou detail ou autres
                    /* */
                    $category_id = $Default_cat_carousel;
                    $category = get_queried_object();
                    $category_nbposts = $category->count;
                    $category_name = get_the_category_by_ID($Default_cat_carousel);
                    $title = $category_name . "/id=" . $category_id . "/nb=" . $category_nbposts . "default";
                    $title = $category_name;
                    $cat = $category_id;
                }
            }
            //$title ="ADOPTE : ".$current_category." ID : ".$cat ;
        }
        //------------------------------------------------------------------------------
        //$max_slides =5;  //exit("Nakrah el arabes");
        //global $post:
        //global $wp_query;           
        $args = array(
            'post_type' => array('post'),
            'taxonomy' => 'category',
            'cat' => $cat,
            //  'category_name' => $category_name,
            'posts_per_page' => 10,//$max_slides
            'meta_key' => 'a_la_une', //$meta_key
            'meta_value' => 1,//$meta_value,
            'orderby' => 'poste_date',
            'order' => 'DESC'
        );
        //echo "<pre>";print_r($args) ;echo "</pre>";
        $alaune_post = new WP_Query($args);
        if ($alaune_post->have_posts()) {
            $nbposts = count($alaune_post->posts); //exit ('oui'.$nbposts);
            // echo "<pre>";print_r($alaune_post);echo "</pre>";  exit ('oui'.$nbposts);   carousel-fade
            if ($nbposts < $max_slides) {
                $max_slides = $nbposts;
            }
        }
        $thumb_default_image = "images/logo-kalimates.png";
        ?>
        <div class="card" style="">
            <?php if (!empty($title)) { ?>
                <div class="card-header">
                    <?php echo $args['before_widget']; ?>
                    <?php echo $args['before_title'] . "<h3 class='panel-title'>" . $title . "</h3>" . $args['after_title']; ?>
                </div>
            <?php } ?>

            <?php
            //if ( has_post_thumbnail() ) {
            //the_post_thumbnail('thumbnail', array('alt' => ''.get_the_title().'', 'title' => ''.get_the_title().''));
            //the_post_thumbnail('large', array('class' => 'pull-left img-thumbnail img-list', 'alt' => ''.get_the_title().'', 'title' => ''.get_the_title().''));
            //}else{
            //echo '<img class="pull-left img-thumbnail" src="'.get_bloginfo( 'stylesheet_directory' ).'/'.$img_default_image.'"  width="140" height="105" alt="'.get_the_title().'" title=" '.get_the_title().'"/>';
            //}
            ?>		

            <div id="demo" data-ride="carousel"  class="carousel  <?php echo $images_transition; ?>  <?php echo $effects; ?>" data-interval="<?php echo $vitesse; ?>">

                <?php if ($carousel_indicators == "OUI") { ?>  
                    <ul class="carousel-indicators">
                        <?php for ($k = 0; $k < $max_slides; $k++) { ?>

                            <li data-target="#demo" data-slide-to="<?php echo $k; ?>" <?php
                            if ($k == 0) {
                                echo " class=\"active\"";
                            }
                            ?>></li>
                            <?php } ?>
                    </ul> 
                <?php }  // $carousel_indicators   ?>

                <div class="carousel-inner">
                    <?php
                    $i = 0;
                    while ($alaune_post->have_posts() && $i < $max_slides) : $alaune_post->the_post();
                        ?>
                        <?php
                        if (!has_post_thumbnail($post->ID)) {
                            continue;
                        }
                        ?> 

                        <div class="carousel-item <?php
                        if ($i == 0) {
                            echo "active";
                        }
                        ?>">
                                 <?php
                                 $url_thumb = wp_get_attachment_thumb_url(get_post_thumbnail_id($post->ID), 'thumbnail');
                                 $url_large = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large');

                                 if (has_post_thumbnail($post->ID)) {
                                     ?> <?php //echo $url_full[0];  ?>
                                <a class="entry-thumbnail" href="<?php the_permalink(); ?>" target="_SELF">
                                    <?php
                                    echo '<img class=" ' . $images_style . '" src="' . $url_large[0] . '"  width="640" height="480" />'; //ok
                                    ?>
                                </a>



                                <?php if ($carousel_texte == "OUI") { ?> 
                                    <div class="carousel-caption">
                                        <div class="titre b">
                                        <span class="univers">
                                            <?php echo get_the_term_list($post->ID, 'univers', '&nbsp;' . _e('&nbsp;', 'kalimates'), ', '); ?>
                                        </span>
                                        <a class="titre" href="<?php the_permalink(); ?>"  target="_SELF">
                                            <?php the_title(); ?>
										</a>	
                                        </div>
                                    </div>

                                <?php } ?> 
                            <?php } ?> 
                        </div>
                        <?php
                        $i++;
                    endwhile;
                    ?>
                    <?php wp_reset_query(); ?>
                </div>
                <?php if ($carousel_controls == "OUI") { ?> 
                    <a class="carousel-control-prev" href="#demo" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next" href="#demo" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                    </a>
                <?php } ?> 
            </div>
        </div>
        <?php
        echo $args['after_widget'];
        $max_slides = $cat = $meta_key = $meta_value = $vitesse = $effects = $images_style = $images_transition = $carousel_indicators = $carousel_texte = $carousel_controls = "";
        //echo"<pre>";print_r($instance);echo "</pre>";
    }

//------------------------------------------------------------------------------		
// Widget Backend 
    public function form($instance) {

//global $args; extract($args);echo "<pre>";print_r($args);echo "</pre>";//widget_id OK

        if (isset($instance['ar_title'])) {
            $ar_title = $instance['ar_title'];
        } else {
            $ar_title = __('', 'kalimates');
        }
        if (isset($instance['fr_title'])) {
            $fr_title = $instance['fr_title'];
        } else {
            $fr_title = __('', 'kalimates');
        }
        if (isset($instance['en_title'])) {
            $en_title = $instance['en_title'];
        } else {
            $en_title = __('', 'kalimates');
        }

        //------------------------------------------------------------------
        //if ( isset( $instance[ 'title' ] ) ) {$title = $instance[ 'title' ];
        //}else {$title = __( 'New title', 'kalimates' );
        //}

        if (isset($instance['max_slides'])) {
            $max_slides = $instance['max_slides'];
        } else {
            $max_slides = __('4', 'kalimates');
        }

        if (isset($instance['meta_key'])) {
            $meta_key = $instance['meta_key'];
        } else {
            $meta_key = __('', 'kalimates');
        }

        if (isset($instance['meta_value'])) {
            $meta_value = $instance['meta_value'];
        } else {
            $meta_value = __('', 'kalimates');
        }

        if (isset($instance['cat'])) {
            $cat = $instance['cat'];
        } else {
            $cat = __('1', 'kalimates');
        }

        if (isset($instance['vitesse'])) {
            $vitesse = $instance['vitesse'];
        } else {
            $vitesse = __('4000', 'kalimates');
        }
        if (isset($instance['effects'])) {
            $effects = $instance['effects'];
        } else {
            $effects = __('', 'kalimates');
        }
        if (isset($instance['images_style'])) {
            $images_style = $instance['images_style'];
        } else {
            $images_style = __('', 'kalimates');
        }

        //nouveaux
        if (isset($instance['images_transition'])) {
            $images_transition = $instance['images_transition'];
        } else {
            $images_transition = __('New images_transition', 'kalimates');
        }
        if (isset($instance['carousel_indicators'])) {
            $carousel_indicators = $instance['carousel_indicators'];
        } else {
            $carousel_indicators = __('New carousel_indicators', 'kalimates');
        }
        if (isset($instance['carousel_controls'])) {
            $carousel_controls = $instance['carousel_controls'];
        } else {
            $carousel_controls = __('New carousel_controls', 'kalimates');
        }
        if (isset($instance['carousel_texte'])) {
            $carousel_texte = $instance['carousel_texte'];
        } else {
            $carousel_texte = __('New carousel_texte', 'kalimates');
        }
        // Widget admin form
        //print_r($instance);
        //global $widget_instance;
        //print_r($args);
        // $widget_id necessaire pour le multiples widgets pour que leurs id # marche	
        $id = explode("-", $this->get_field_id("widget_id"));
        $widget_id = $id[1] . "-" . $id[2];
        ?>

        <div class="web4-carousel">
            <ul class="nav nav-pills">
                <li class="btn btn-outline-secondary btn-sm"><a data-toggle="tab"  href="#title_<?php echo $widget_id; ?>">TITLES</a></li>
                <li class="btn btn-outline-secondary btn-sm"><a data-toggle="tab" href="#params_<?php echo $widget_id; ?>">PARAMS</a></li>
                <li class="btn btn-outline-secondary btn-sm"><a data-toggle="tab" href="#style_<?php echo $widget_id; ?>">STYLES</a></li>
            </ul>
            <div class="tab-content"><!--  fade in active -->
                <div id="title_<?php echo $widget_id ; ?>" class="tab-pane active">
                    <h3>TITLES</h3>
                    <div class="">
                        <label for="<?php echo $this->get_field_id('ar_title'); ?>"><?php _e('Arabic Title'); ?></label> 
                        <input class="widefat" id="<?php echo $this->get_field_id('ar_title'); ?>" name="<?php echo $this->get_field_name('ar_title'); ?>" type="text" value="<?php echo esc_attr($ar_title); ?>" />
                        <label for="<?php echo $this->get_field_id('fr_title'); ?>"><?php _e('French Title'); ?></label> 
                        <input class="widefat" id="<?php echo $this->get_field_id('fr_title'); ?>" name="<?php echo $this->get_field_name('fr_title'); ?>" type="text" value="<?php echo esc_attr($fr_title); ?>" />
                        <label for="<?php echo $this->get_field_id('en_title'); ?>"><?php _e('English Title'); ?></label> 
                        <input class="widefat" id="<?php echo $this->get_field_id('en_title'); ?>" name="<?php echo $this->get_field_name('en_title'); ?>" type="text" value="<?php echo esc_attr($en_title); ?>" />
                        <br>
					</div>
                </div>
                <div id="params_<?php echo $widget_id; ?>" class="tab-pane">
                    <h3>PARAMS</h3>
                    <div class="">
                        <p>
                            <label for="<?php echo $this->get_field_id('max_slides'); ?>"><?php _e('max_slides'); ?>(dafault = 4)</label> 
                            <input size="6" class="widefat" id="<?php echo $this->get_field_id('max_slides'); ?>" name="<?php echo $this->get_field_name('max_slides'); ?>" type="text" value="<?php echo esc_attr($max_slides); ?>" />
                            <label for="<?php echo $this->get_field_id('meta_key'); ?>"><?php _e('meta_key'); ?> (dafault = )</label> 
                            <input size="6" class="widefat" id="<?php echo $this->get_field_id('meta_key'); ?>" name="<?php echo $this->get_field_name('meta_key'); ?>" type="text" value="<?php echo esc_attr($meta_key); ?>" />
                            <label for="<?php echo $this->get_field_id('meta_value'); ?>"><?php _e('meta_value'); ?>(dafault = )  </label> 
                            <input size="6" class="widefat" id="<?php echo $this->get_field_id('meta_value'); ?>" name="<?php echo $this->get_field_name('meta_value'); ?>" type="text" value="<?php echo esc_attr($meta_value); ?>" />

                            <label for="<?php echo $this->get_field_id('cat'); ?>"><?php _e('Category'); ?> </label> <?php
                            $args = array(
                                'name' => $this->get_field_name('cat'),
                                'show_option_none' => __('Select category'),
                                'show_count' => 1,
                                'orderby' => 'name',
                                'echo' => 1,
                                //'selected'         => $category,
                                'selected' => $cat,
                                'class' => 'widefat'
                            );
                            wp_dropdown_categories($args);
                            ?>
                            <label for="<?php echo $this->get_field_id('vitesse'); ?>"><?php _e('vitesse'); ?> (dafault = 2000 | eq 2 secondes|)</label> 
                            <input  size="6" class="widefat" id="<?php echo $this->get_field_id('vitesse'); ?>" name="<?php echo $this->get_field_name('vitesse'); ?>" type="text" value="<?php echo esc_attr($vitesse); ?>" />
                        </p>
                    </div>
                </div>
                <div id="style_<?php echo $widget_id; ?>" class="tab-pane">
                    <h3>STYLES</h3>
                    <div class="">
                        <p>

                            <label for="<?php echo $this->get_field_id('effects'); ?>"><?php _e('Images Effect'); ?> </label> 
                            <select class="widefat" name="<?php echo $this->get_field_name('effects'); ?>"   id="<?php echo $this->get_field_id('effects'); ?>">
                                <option value="" <?php
                                if ($effects == "") {
                                    echo "selected";
                                }
                                ?> >Normal</option>
                                <option value="carousel-sepia" <?php
                                if ($effects == "carousel-sepia") {
                                    echo "selected";
                                }
                                ?> >Sepia</option>
                                <option value="carousel-grayscale" <?php
                                if ($effects == "carousel-grayscale") {
                                    echo "selected";
                                }
                                ?>>Grayscale</option>
                                <option value="carousel-contrast" <?php
                                if ($effects == "carousel-contrast") {
                                    echo "selected";
                                }
                                ?>>Contrast</option>
                                <option value="carousel-saturate" <?php
                                if ($effects == "carousel-saturate") {
                                    echo "selected";
                                }
                                ?>>Saturate</option>
                                <option value="carousel-blur" <?php
                                if ($effects == "carousel-blur") {
                                    echo "selected";
                                }
                                ?>>Blur</option>
                            </select>

                            <label for="<?php echo $this->get_field_id('images_style'); ?>"><?php _e('Images Style'); ?> </label> 
                            <select class="widefat" name="<?php echo $this->get_field_name('images_style'); ?>"   id="<?php echo $this->get_field_id('images_style'); ?>">
                                <option value="img-rounded" <?php
                                if ($images_style == "img-rounded") {
                                    echo "selected";
                                }
                                ?> >rounded</option>
                                <option value="img-circle" <?php
                                if ($images_style == "img-circle") {
                                    echo "selected";
                                }
                                ?> >circle</option>
                                <option value="img-thumbnail" <?php
                                if ($images_style == "img-thumbnail") {
                                    echo "selected";
                                }
                                ?>>thumbnail</option>
                            </select>

                            <label for="<?php echo $this->get_field_id('images_transition'); ?>"><?php _e('Images Transition'); ?> </label> 
                            <select class="widefat" name="<?php echo $this->get_field_name('images_transition'); ?>"   id="<?php echo $this->get_field_id('images_transitions'); ?>">
                                <option value="slide" <?php
                                if ($images_transition == "slide") {
                                    echo "selected";
                                }
                                ?> >slide</option>
                                <option value="carousel-fade" <?php
                                if ($images_transition == "carousel-fade") {
                                    echo "selected";
                                }
                                ?> >fade</option>
                                <option value="carousel-rotate" <?php
                                if ($images_transition == "carousel-rotate") {
                                    echo "selected";
                                }
                                ?> >rotate</option>
                                <option value="carousel-zoom" <?php
                                if ($images_transition == "carousel-zoom") {
                                    echo "selected";
                                }
                                ?> >zoom</option>
                            </select>

                            <label for="<?php echo $this->get_field_id('carousel_indicators'); ?>"><?php _e('Carosel Indicators'); ?> </label> 
                            <select class="widefat" name="<?php echo $this->get_field_name('carousel_indicators'); ?>"   id="<?php echo $this->get_field_id('carousel_indicators'); ?>">
                                <option value="OUI" <?php
                                if ($carousel_indicators == "OUI") {
                                    echo "selected";
                                }
                                ?> >OUI</option>
                                <option value="NON" <?php
                                if ($carousel_indicators == "NON") {
                                    echo "selected";
                                }
                                ?> >NON</option>
                            </select>

                            <label for="<?php echo $this->get_field_id('carousel_controls'); ?>"><?php _e('Carosel Controls'); ?> </label> 
                            <select class="widefat" name="<?php echo $this->get_field_name('carousel_controls'); ?>"   id="<?php echo $this->get_field_id('carousel_controls'); ?>">
                                <option value="OUI" <?php
                                if ($carousel_controls == "OUI") {
                                    echo "selected";
                                }
                                ?> >OUI</option>
                                <option value="NON" <?php
                                if ($carousel_controls == "NON") {
                                    echo "selected";
                                }
                                ?> >NON</option>
                            </select>

                            <label for="<?php echo $this->get_field_id('carousel_texte'); ?>"><?php _e('Carosel Texte'); ?> </label> 
                            <select class="widefat" name="<?php echo $this->get_field_name('carousel_texte'); ?>"   id="<?php echo $this->get_field_id('carousel_texte'); ?>">
                                <option value="OUI" <?php
                                if ($carousel_texte == "OUI") {
                                    echo "selected";
                                }
                                ?> >OUI</option>
                                <option value="NON" <?php
                                if ($carousel_texte == "NON") {
                                    echo "selected";
                                }
                                ?> >NON</option>
                            </select>
                        </p>
                    </div>
                </div>
            </div>
        </div>


        <?php
    }

//------------------------------------------------------------------------------	
// Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {
        $instance = array();
        //$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['ar_title'] = (!empty($new_instance['ar_title']) ) ? strip_tags($new_instance['ar_title']) : '';
        $instance['fr_title'] = (!empty($new_instance['fr_title']) ) ? strip_tags($new_instance['fr_title']) : '';
        $instance['en_title'] = (!empty($new_instance['en_title']) ) ? strip_tags($new_instance['en_title']) : '';
        //$instance['my_code'] = ( ! empty( $new_instance['my_code'] ) ) ? strip_tags( $new_instance['my_code'] ) : '';
        $instance['max_slides'] = (!empty($new_instance['max_slides']) ) ? strip_tags($new_instance['max_slides']) : '';
        $instance['meta_key'] = (!empty($new_instance['meta_key']) ) ? strip_tags($new_instance['meta_key']) : '';
        $instance['meta_value'] = (!empty($new_instance['meta_value']) ) ? strip_tags($new_instance['meta_value']) : '';
        $instance['cat'] = (!empty($new_instance['cat']) ) ? strip_tags($new_instance['cat']) : '';

        $instance['vitesse'] = (!empty($new_instance['vitesse']) ) ? strip_tags($new_instance['vitesse']) : '';
        $instance['effects'] = (!empty($new_instance['effects']) ) ? strip_tags($new_instance['effects']) : '';
        $instance['images_style'] = (!empty($new_instance['images_style']) ) ? strip_tags($new_instance['images_style']) : '';

        $instance['images_transition'] = (!empty($new_instance['images_transition']) ) ? strip_tags($new_instance['images_transition']) : '';
        $instance['carousel_indicators'] = (!empty($new_instance['carousel_indicators']) ) ? strip_tags($new_instance['carousel_indicators']) : '';
        $instance['carousel_controls'] = (!empty($new_instance['carousel_controls']) ) ? strip_tags($new_instance['carousel_controls']) : '';
        $instance['carousel_texte'] = (!empty($new_instance['carousel_texte']) ) ? strip_tags($new_instance['carousel_texte']) : '';

        return $instance;
    }

}
//------------------------------------------------------------------------------
add_action('wp_enqueue_scripts', 'web4_widget_carousel_css');
function web4_widget_carousel_css() {
    wp_enqueue_style('bootstrap-min', plugins_url('/css/bootstrap.min.css', __FILE__));
    wp_enqueue_style('web4-carousel', plugins_url('/css/carousel.css', __FILE__));
}
//------------------------------------------------------------------------------
add_action('init', 'web4_widget_carousel_js');
function web4_widget_carousel_js() {
	//wp_register_script( $handle, $src, $deps, $ver, $in_footer );
	// $jQuery = 'http://code.jquery.com/jquery-latest.min.js';
	if ( ! wp_script_is( 'jquery', 'enqueued' )) {
        wp_enqueue_script( 'jquery' );
		wp_register_script('bootstrap-js', plugins_url('/js/bootstrap.min.js', __FILE__), array('jquery'),true);
		wp_enqueue_script('bootstrap-js');
    }
}
//------------------------------------------------------------------------------
// Register and load the widget
function web4_widget_carousel_init() {
    register_widget('web4_widget_carousel');
}
add_action('widgets_init', 'web4_widget_carousel_init');
//add_filter('widget_text', 'do_shortcode', 11);
//------------------------------------------------------------------------------

function web4_widget_carousel_jsenline($instance) {
    if (wp_script_is('jquery')) {
        ?>
        <!-- caroussel-->
        <script>
            $('.carousel').carousel({
                interval: 5000
            })
        </script>
        <!-- /caroussel -->
        <?php
    }
}
add_action('wp_footer', 'web4_widget_carousel_jsenline');
//------------------------------------------------------------------------------
// modifier les css admin
add_action('admin_head', 'web4_carousel_admin_css');
function web4_carousel_admin_css() {
    echo '<style>
.web4-carousel {
	padding : 10px;
	}
.web4-carousel ul{
    /*border: thin solid #ff0000!important;
    font-size: 0.8em;
    font-color:black!important;
    background-color: pink!important;*/
}
.web4-carousel ul li{
       margin:5px!important;
    }
.fade:not(.show) {
    opacity: 1!important;
	}
  </style>';
}
//------------------------------------------------------------------------------
//TROUIVER La soulution
//add_shortcode( 'plan_du_site', 'web4_widget_carousel->widget' );
?>
